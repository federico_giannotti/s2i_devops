const app = require("./ecosystem.BASE.config.js")

module.exports = {
    apps : [
      {
        ...app,
        name: "PROD - " + app.name,
        env: {
          ...app.env,
          NODE_ENV: 'production',
          PORT: 3010
        }
      }
    ],
  };