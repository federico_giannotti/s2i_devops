let app = {
      name: 'API - Start2impact',
      script: './app.js',
      watch: true,
      ignore_watch : ["node_modules"],
      restart_delay: 5000,
      autorestart: true,
      output: '/dev/null',
      error: '/dev/null',
      log: '/dev/null',
      env: {
          NODE_ENV: 'development',
          PORT: 3000,
      }
    }

module.exports = app