const app = require("./ecosystem.BASE.config.js")

module.exports = {
    apps : [
      {
        ...app,
        name: "TEST - " + app.name,
        env: {
          ...app.env,
          NODE_ENV: 'production',
          PORT: 3010
        }
      }
    ],
  };